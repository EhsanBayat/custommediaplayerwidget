package com.pasargad.mediaplayerwidget.base;

public enum PlaybackStatus {
    PLAYING,
    PAUSED
}