package com.pasargad.mediaplayerwidget.base;

public class Constants {

    private static final String FONTS_ROOT                                 = "fonts/";
    public static final String FONT_NUMBER                                 = FONTS_ROOT +"iransans_number.ttf";
}
