package com.pasargad.mediaplayerwidget.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AudioBean implements Parcelable {

    private String data;
    private String title;
    private String album;
    private String artist;

    public AudioBean(String data, String title, String album, String artist) {
        this.data = data;
        this.title = title;
        this.album = album;
        this.artist = artist;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    protected AudioBean(Parcel in) {
        data = in.readString();
        title = in.readString();
        album = in.readString();
        artist = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(data);
        dest.writeString(title);
        dest.writeString(album);
        dest.writeString(artist);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AudioBean> CREATOR = new Parcelable.Creator<AudioBean>() {
        @Override
        public AudioBean createFromParcel(Parcel in) {
            return new AudioBean(in);
        }

        @Override
        public AudioBean[] newArray(int size) {
            return new AudioBean[size];
        }
    };
}