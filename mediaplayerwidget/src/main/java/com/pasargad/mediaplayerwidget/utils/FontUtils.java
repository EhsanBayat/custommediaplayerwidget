package com.pasargad.mediaplayerwidget.utils;

import android.content.Context;
import android.graphics.Typeface;
import com.pasargad.mediaplayerwidget.base.Constants;

public final class FontUtils {

    private FontUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static Typeface getFont(Context context) {
        return FontManager.getTypeFace(context, Constants.FONT_NUMBER);
    }
    public static class FontManager {
        static Typeface getTypeFace(Context context, String font) {
            return Typeface.createFromAsset(context.getAssets(), font);
        }
    }
}
