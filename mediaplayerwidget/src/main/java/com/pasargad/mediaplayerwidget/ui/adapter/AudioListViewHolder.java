package com.pasargad.mediaplayerwidget.ui.adapter;

import android.content.Context;
import android.view.View;

import com.pasargad.mediaplayerwidget.ui.adapter.genericadapter.BaseViewHolder;
import com.pasargad.mediaplayerwidget.ui.adapter.genericadapter.OnRecyclerItemClickListener;
import com.pasargad.mediaplayerwidget.model.AudioBean;
import com.pasargad.mediaplayerwidget.ui.widgets.CustomTextView;
import com.transportation.mediaplayerwidget.R;

public class AudioListViewHolder extends BaseViewHolder<AudioBean, OnRecyclerItemClickListener> {

    private CustomTextView txtTitle;
    private CustomTextView txtArtist;
    private CustomTextView txtAlbum;

    private Context context;

    public AudioListViewHolder(Context context , View itemView, OnRecyclerItemClickListener listener) {
        super(itemView, listener);
        this.context = context;
        txtTitle = itemView.findViewById(R.id.txtTitle);
        txtArtist = itemView.findViewById(R.id.txtArtist);
        txtAlbum = itemView.findViewById(R.id.txtAlbum);
    }

    @Override
    public void onBind(AudioBean bean) {

        txtTitle.setText(bean.getTitle());
        txtArtist.setText(bean.getArtist());
        txtAlbum.setText(bean.getAlbum());

        itemView.setOnClickListener(v -> {
            if(getListener() != null)
                getListener().onItemClick(getAdapterPosition());
        });
    }

}
