package com.pasargad.mediaplayerwidget.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;
import com.pasargad.mediaplayerwidget.ui.adapter.genericadapter.GenericRecyclerViewAdapter;
import com.pasargad.mediaplayerwidget.ui.adapter.genericadapter.OnRecyclerItemClickListener;
import com.pasargad.mediaplayerwidget.model.AudioBean;
import com.transportation.mediaplayerwidget.R;

public class AudioListAdapter extends GenericRecyclerViewAdapter<AudioBean, OnRecyclerItemClickListener, AudioListViewHolder> {

    private Context context;
    public AudioListAdapter(Context context, OnRecyclerItemClickListener listener) {
        super(context, listener);
        this.context = context;
    }
    @Override
    public AudioListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AudioListViewHolder(context,inflate(R.layout.row_audio_item, parent), getListener());
    }
}
