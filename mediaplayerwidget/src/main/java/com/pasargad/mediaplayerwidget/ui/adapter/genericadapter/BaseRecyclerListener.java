package com.pasargad.mediaplayerwidget.ui.adapter.genericadapter;

/**
 * Base recycler click listener contract.
 * To be implemented by all the click listeners in order to be used with the
 * {@link GenericRecyclerViewAdapter}
 */
public interface BaseRecyclerListener {
}
