package com.pasargad.mediaplayerwidget.ui.screens;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.pasargad.mediaplayerwidget.service.MediaPlayerService;
import com.pasargad.mediaplayerwidget.ui.adapter.AudioListAdapter;
import com.pasargad.mediaplayerwidget.utils.StorageUtil;
import com.transportation.mediaplayerwidget.R;
import com.pasargad.mediaplayerwidget.model.AudioBean;
import java.util.ArrayList;
import java.util.Random;

public class MediaPlayerActivity extends AppCompatActivity {

    private RecyclerView rvMedias;

    public static final String Broadcast_PLAY_NEW_AUDIO = "com.pasargad.mediaplayerwidget.PlayNewAudio";
    private MediaPlayerService player;
    private boolean serviceBound = false;
    private ArrayList<AudioBean> audioList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);

        rvMedias = findViewById(R.id.rvMedias);

        loadAudio();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    private void loadAudio() {
        ContentResolver contentResolver = getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            audioList = new ArrayList<>();
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                audioList.add(new AudioBean(data, title, album, artist));
            }
        }
        cursor.close();

        if(audioList.size() > 0)
            loadAdapter();
    }

    private void loadAdapter() {
        LinearLayoutManager lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        AudioListAdapter audioListAdapter= new AudioListAdapter(this,this::playAudio);
        rvMedias.setLayoutManager(lm);
        rvMedias.setAdapter(audioListAdapter);
        audioListAdapter.setItems(audioList , true);

        playAudio(getRandomElement(audioList));
    }

    public int getRandomElement(ArrayList<AudioBean> list)
    {
        Random rand = new Random();
        return rand.nextInt(list.size());
    }

    private void playAudio(int audioIndex) {
        if (!serviceBound) {
            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudio(audioList);
            storage.storeAudioIndex(audioIndex);

            Intent playerIntent = new Intent(this, MediaPlayerService.class);
            startService(playerIntent);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudioIndex(audioIndex);

            Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
            sendBroadcast(broadcastIntent);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("ServiceState", serviceBound);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean("ServiceState");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (serviceBound) {
            unbindService(serviceConnection);

            player.stopSelf();
        }
    }

}