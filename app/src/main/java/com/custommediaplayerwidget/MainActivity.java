package com.custommediaplayerwidget;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;

import com.pasargad.mediaplayerwidget.ui.screens.MediaPlayerActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnOpenPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnOpenPlayer = findViewById(R.id.btnOpenPlayer);
        btnOpenPlayer.setOnClickListener(v -> {
            if(PermissionUtil.isReadStoragePermissionGranted(this))
                startActivity(new Intent(this , MediaPlayerActivity.class));
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 3) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                btnOpenPlayer.performClick();
            } else {
                PermissionUtil.requestReadStoragePermission(this);
            }
        }
    }
}